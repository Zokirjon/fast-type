package uz.azn.homework

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.HandlerThread
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_texts.*
import uz.azn.homework.databinding.ActivityTextsBinding
import java.util.*
import java.util.logging.Handler
import kotlin.text.StringBuilder

class TextsActivity : AppCompatActivity() {

    var COUNT_DOWN_TIMER_TEXT = 120000
    var COUNT_DOWN_TIMER_WORD = 60000

    private var wordslist = WordsAndTexts.wordList
    private var textlist = WordsAndTexts.textList

    private var generationList: MutableList<Int> = arrayListOf()

    var counter = 1

    var countOne = 0


    val binding by lazy { ActivityTextsBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        var mTimeLeftInMillis = COUNT_DOWN_TIMER_TEXT.toLong()
        var mTimeLeftInMillisTwo = COUNT_DOWN_TIMER_WORD.toLong()

        val activityMain = activityMain()
        if (activityMain) countTimer(mTimeLeftInMillis)
        else countTimer(mTimeLeftInMillisTwo)


    }

    fun activityMain(): Boolean {
        val booleanExtra = intent.getBooleanExtra("text", false)
        Log.d("MainActivity", booleanExtra.toString())
        if (booleanExtra) {
            binding.wordTextView.text = "word:"
            editText(textlist)

        } else {
            binding.wordTextView.text = "word:"
            editText(wordslist)
        }
        return booleanExtra
    }

    fun countTimer(mTimeLeftInMillis: Long) {

        var a = mTimeLeftInMillis
        object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onFinish() {
                alertDialog("Vaqtizgiz tugadi ", counter)
                binding.writeEditText.visibility = View.INVISIBLE
                val number = mTimeLeftInMillis.toInt()
                val minuts = number / 1000 / 60
                val second = number / 1000 % 60
                val timeLeftFormatted =
                    String.format(Locale.getDefault(), "%02d:%02d", minuts, second)
                binding.timeClickTextView.text = timeLeftFormatted
            }

            override fun onTick(millisUntilFinished: Long) {
                a = millisUntilFinished
                upDateCountDownTimer(a)
            }
        }.start()
    }

    fun upDateCountDownTimer(timeLong: Long) {
        val number = timeLong.toInt()
        val minuts = number / 1000 / 60
        val second = number / 1000 % 60
        val timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minuts, second)
        binding.timeClickTextView.text = timeLeftFormatted
    }


    fun randomText(): Int {
        var random = (wordslist.indices).random()
        while (random in generationList) {
            random = (textlist.indices).random()
        }
        generationList.add(random)
        if (generationList.size <= wordslist.size) return random
        else {
            alertDialog("Siz Yutingiz:", countOne)

        }
        return 0
    }

    fun editText(list: MutableList<String>) {
        val random = randomText()
        binding.wordText.text = list[random]
        binding.writeEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(
                s: Editable?
            ) {

            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
                binding.wordCountTextView.text = (counter).toString()

            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (s!!.contains(list[random])) {
                    counter++
                    countOne++

                    binding.writeEditText.setTextColor(Color.GREEN)

                    android.os.Handler().postDelayed(Runnable {
                        binding.writeEditText.text.clear()
                        binding.wordCountTextView.text = counter.toString()
                        activityMain()
                    }, 1000)


                } else binding.writeEditText.setTextColor(Color.RED)

                binding.letterCountTextView.text = s?.length.toString()

            }

        })
    }

    fun alertDialog(name: String, count: Int) {
        val builder = AlertDialog.Builder(this@TextsActivity)
        builder.setTitle("Fast Type")
            .setMessage("$name $count ga javob beridingiz ")
            .setPositiveButton("Ok") { dialog, which ->
                dialog.apply {
                    finish()
                }
            }
        builder.show()
    }

}